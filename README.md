# Actividad 3: Pokémons

Este proyecto consiste en añadir estilos al documento HTML proporcionado para mostrar información sobre diferentes pokémones. A continuación, se detallan los requisitos y las restricciones de la actividad:

## Requisitos de la Actividad

1. Añadir estilos al documento HTML proporcionado.
2. Para dispositivos hasta 600px de ancho, se mostrará una columna.
3. Para dispositivos entre 601px y 800px de ancho, se mostrarán dos columnas.
4. Para dispositivos mas grandes de 800px, se mostrarán cuatro columnas.
5. Cada pokémon debe mostrar la siguiente información:
   - La primera letra del nombre en mayúsculas.
   - Cada dato debe mostrarse con su correspondiente texto delante o detrás según se especifica:
     - Code: delante del número del pokémon.
     - Type: delante de la lista <ul>. Separa entre comas cada uno de los valores de la lista, excepto el último.
     - Height: delante de la altura, seguido de "m" después.
     - Weight: delante del peso, seguido de "kg" después.

```Nota: si tienes que añadir espacios en blanco utiliza el código Unicode \u00a0```

## Estilos Personalizados

Para obtener los gradientes dorados y plateados requeridos, se pueden utilizar herramientas en línea como [CSS Gradient](https://cssgradient.io/).

## Estructura del Repositorio

- **img/** Carpeta que contine imagenes de antes y después.
- **index.html:** Archivo HTML proporcionado con la estructura básica de la página.
- **styles.css:** Archivo CSS donde se añadirán los estilos necesarios para cumplir con los requisitos de la actividad.

## Capturas de Pantalla

Capturas de pantalla para mostrar cómo ha quedado el proyecto. Están ubicadas en la carpeta `img`.

- **Captura de Resultados sin Estilos CSS Aplicados:**
  ![Captura de Resultados sin Estilos CSS Aplicados](img/html_sin_css.png)
  
- **Captura de Resultados para Dispositivos mayores de 800px:**
  ![Captura de Resultados para Dispositivos mayores de 800px](img/mas_de_800px.png)

- **Captura de Resultados para Dispositivos entre 601px y 800px:**
  ![Captura de Resultados para Dispositivos entre 601px y 800px](img/entre_601_800px.png)

- **Captura de Resultados para Dispositivos hasta 600px:**
  ![Captura de Resultados para Dispositivos hasta 600px](img/hasta_600px.png)


## Estado

[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=success)](https://gitlab.com/gusgonza/MP04-actividad-3/-/tree/main)
